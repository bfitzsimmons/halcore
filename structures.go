package halcore

// Self is the "self" property of the "_links" JSON substructure present in all API response.
type Self struct {
	Href   string `json:"href,omitempty"`
	Method string `json:"method,omitempty"`
}

// Next is the "next" property of the "_links" JSON substructure present in all API response.
type Next struct {
	Href string `json:"href,omitempty"`
}

// Previous is the "previous" property of the "_links" JSON substructure present in all API response.
type Previous struct {
	Href string `json:"href,omitempty"`
}

// Find is the "find" property of the "_links" JSON substructure present in all API response.
type Find struct {
	Href      string `json:"href,omitempty"`
	Templated bool   `json:"templated,omitempty"`
	Method    string `json:"method,omitempty"`
}

// Search is the "search" property of the "_links" JSON substructure present in all API response.
type Search struct {
	Href      string `json:"href,omitempty"`
	Templated bool   `json:"templated,omitempty"`
}

// Docs is the "docs" property of the "_links" JSON substructure present in all API response.
type Docs struct {
	Href string `json:"href"`
}

// Schema is the "schema" property of the "_links" JSON substructure present in all API response.
type Schema struct {
	Href string `json:"href"`
}

// Create is the "create" property of the "_links" JSON substructure present in all API response.
type Create struct {
	Href   string `json:"href,omitempty"`
	Method string `json:"method,omitempty"`
}

// Read is the "read" property of the "_links" JSON substructure present in all API response.
type Read struct {
	Href      string `json:"href"`
	Templated bool   `json:"templated,omitempty"`
}

// Update is the "update" property of the "_links" JSON substructure present in all API response.
type Update struct {
	Href   string `json:"href,omitempty"`
	Method string `json:"method,omitempty"`
}

// Delete is the "delete" property of the "_links" JSON substructure present in all API response.
type Delete struct {
	Href   string `json:"href,omitempty"`
	Method string `json:"method,omitempty"`
}
